import java.util.*;

public class SimpleQLearningApp {
	public static void main(String[] args) {
        int[][] r = new int[][] {
        		{-1, -1, -1, -1, 0, -1},
        		{-1, -1, -1, 0, -1, 100},
        		{-1, -1, -1, 0, -1, -1},
        		{-1, 0, 0, -1, 0, -1},
        		{0, -1, -1, 0, -1, 100},
        		{-1, 0, -1, -1, 0, 100},
        };
        
        Agent a = new Agent(6, 0.8, 5);
        a.train(r, 1000);
        // get path when starting from room 2
        a.getPath(2);
    }
}

import java.util.*;

public class Agent {
	private int[][] q;
	private double gamma;
	private int rooms;
	private int episodes;
	private int goal;
	
	private Random rand = new Random();
	
	// main agent constructor
	public Agent(int rooms, double gamma, int goal) {
		this.rooms = rooms;
		this.gamma = gamma;
		this.episodes = 100;
		this.goal = goal;
		// the brain of the agent, this is built during training
		q = new int[rooms][rooms];
	}
	
	// main training method for building up the q array of the agent
	public void train(int[][] r, int episodes) {
		// min of 100 episodes
		if (episodes > this.episodes)
			this.episodes = episodes;
		
		ArrayList<Integer> nextRooms = new ArrayList<Integer>();
		ArrayList<Integer> nextNextRooms = new ArrayList<Integer>();
		int next_room;
		
		// starting from every room at least once
		for (int i = 0; i < this.rooms; i++) {
			nextRooms = findRooms(r, i);
			next_room = pickRandomRoom(nextRooms);
			
			nextNextRooms = findRooms(r, next_room);
			
			int maxVal = getMax(next_room, nextNextRooms);
			
			// updating the "brain" of the agent
			this.q[i][next_room] = (int) (r[i][next_room] + this.gamma * maxVal);
		}
		
		int room_start;
		// randomly picking a room to start from
		for (int j = 0; j < this.episodes; j++) {
			room_start = (Math.abs(rand.nextInt())) % this.rooms;
			
			nextRooms = findRooms(r, room_start);
			next_room = pickRandomRoom(nextRooms);
			
			nextNextRooms = findRooms(r, next_room);
			
			int maxVal = getMax(next_room, nextNextRooms);
			
			// updating the "brain" of the agent
			this.q[room_start][next_room] = (int) (r[room_start][next_room] + this.gamma * maxVal);
		}
	}
	
	// the agent uses is q array (brain) to provide a path to the goal
	public void getPath(int start) {
		ArrayList<Integer> path = new ArrayList<Integer>();
		int curr = start;
		int next;
		
		path.add(curr);
		
		while(curr != goal) {
			int temp = q[curr][0];
			next = 0;
			
			for (int i = 1; i < q[0].length; i++) {
				if (temp < q[curr][i]) {
					temp = q[curr][i];
					next = i;
				}	
			}
			
			path.add(next);
			curr = next;
		}
		
		System.out.print(path.get(0));
		
		for (int j = 1; j < path.size(); j++) 
			System.out.print(" -> " + path.get(j));
		
		System.out.println("");
	}

	// get the max value from the q array of the agent given the room
	private int getMax(int next_room, ArrayList<Integer> nextNextRooms) {
		int i = next_room;
		int j = nextNextRooms.get(0);
		
		int max = this.q[i][j];
		
		for (int a = 1 ; a < nextNextRooms.size(); a++) {
			j = nextNextRooms.get(a);
			if (max < this.q[i][j])
				max = this.q[i][j];
		}
		
		return max;
	}

	// randomly pick a room from the list of rooms
	private int pickRandomRoom(ArrayList<Integer> nextRooms) {
		int i = (Math.abs(rand.nextInt())) % (nextRooms.size());
		
		return nextRooms.get(i);
	}

	// find all of the rooms that can be reached from the starting room
	private ArrayList<Integer> findRooms(int[][] r, int i) {
		ArrayList<Integer> res = new ArrayList<Integer>();
		
		for (int a = 0; a < r[0].length; a++) {
			if (r[i][a] == 0 || r[i][a] == 100)
				res.add(a);
		}
		return res;
	}	
}
